<?php

namespace Academy;

use CodaPHP;

class CodaClass
{
    protected $client;
    protected $doc;
    protected $table;
    /**
     * CodaClass constructor.
     * @param string $token Токен
     * @param string $doc ID документа
     * @param string $table Таблица, в которую будет выгружена информация
     */
    public function __construct(string $token, string $doc, string $table)
    {
        $this->client = new CodaPHP\CodaPHP($token);
        $this->doc = $doc;
        $this->table = $table;
    }
    /**
     * @param $data Массив данных
     */
    public function addDataToCoda(array $data)
    {
        
        $arCodaTimes = $this->codaClient->listRows($this->document, $this->table);
        foreach ($arCodaTimes['items'] as $items)
        {
            $id = $items["values"]["ID"];
            if ($id && $data[$id]) {
                unset($data[$id]);
            }
        }
        $ins = $this->client->insertRows(
            $this->doc,
            $this->table,
            $data
        );
        return $ins;
    }
}
