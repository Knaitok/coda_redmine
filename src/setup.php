<?php

namespace Academy;

$setting = 'var/setting.yaml';
use redmine;
use coda;
class setup
{
    protected $codaClient;
    protected $redmineClient;
    /**
     * Synchronization constructor.
     * @param string $config Путь к yaml-файлу с конфигурацией
     */
    public function __construct(string $config)
    {
        // Парсинг yaml-файла
        $setting = yaml_parse($config);
        $settingCoda = $setting['coda'];
        $settingRedmine = $setting['redmine'];

        // Создание объекта класса Coda
        $this->codaClient = $Coda;
        $Coda = new CodaClass(
            $settingCoda['token'],
            $settingCoda['doc'],
            $settingCoda['table']
        );
        // Создание объекта класса Redmine
        $this->redmineClient = $Redmine;
        $Redmine = new RedmineClass(
            $settingRedmineg['period'],
            $settingRedmine['limit'],
            $settingRedmine['url'],
            $settingRedmine['key']
        );
    }
     function syncData()
    {
        $data = $this->getData();
		return $this->addDataToCoda($data);
    }
}